#!/bin/bash
service mysql start
sleep 60
mysql < create_db_table.sql
sed -i 's/127.0.0.1/*/g' /etc/mysql/mysql.conf.d/mysqld.cnf
service mysql restart
sleep 3600
